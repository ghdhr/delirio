﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Delirio
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }
        PreguntasDelirio resultados = new PreguntasDelirio();
        bool flag = false;
        private void frmMain_Load(object sender, EventArgs e)
        {
            lb_ResultDelirio.Text = "No lo se";
        }
        private string CalcResult() {
            //El estado positivo significa que tiene la probabilidad de tener delirio
            bool feature1 = false;
            bool feature2 = false;
            bool feature3 = false;
            bool feature4 = true;


            /// Feature 1
            /// Feature 2
            //existen 3 letras A
            if (!(resultados.countEspacios <= 1 || resultados.countEspacios >= 5))
                feature2 = true;
            /// Feature 3
            if ( float.Parse(resultados.emotionResult[1]) < float.Parse(resultados.emotionResult[2]))// Si esta mas enojado que feliz
            {
                feature3 = true;
            }
            /// Feature 4
            for (int i = 0; i < 4; i++)
            {
                if (resultados.preguntas[i, 1] != resultados.preguntas[i, 2])
                {
                    feature4 = false;
                }
            }
            /// Feature 4.5 mover las manos
            if (resultados.handCheck == false) {
                feature4 = true;
            }
            // Resultado final
            if ((feature1 && feature2) && (feature3 || (feature4)))
                return "Si";
            else
                return "No";
            
        }
        private void btn_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_PSNResult_Click(object sender, EventArgs e)
        {
            frmResultados o = new frmResultados("Preguntas", resultados);
            o.ShowDialog();
        }

        private void btn_PVisualesResult_Click(object sender, EventArgs e)
        {
            frmResultados o = new frmResultados("Mover las manos", resultados);
            o.ShowDialog();
        }

        private void btn_EmotionResult_Click(object sender, EventArgs e)
        {
            frmResultados o = new frmResultados("Emociones", resultados);
            o.ShowDialog();
        }
        private void btn_Iniciar_Click(object sender, EventArgs e)
        {
            frmConsultNombre o = new frmConsultNombre();
            o.ShowDialog();
            if(!flag)
                resultados= o.Callback();
            flag = true;
            lb_ResultDelirio.Text = CalcResult();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////    Eventos Del Formulario   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Inicializar Variables
        Point FormPosition;
        Boolean mouseAction;

        private void Logear_MouseMove(object sender, MouseEventArgs e)
        {
            //Si el formulario fue Clicleado
            if (mouseAction == true)
            {
                //A la locación del formulario darle las cordenadas del mouse
                Location = new Point(Cursor.Position.X - FormPosition.X, Cursor.Position.Y - FormPosition.Y);
            }
        }

        private void Logear_MouseDown(object sender, MouseEventArgs e)
        {
            //Darle las cordenadas del mouse a FormPosition
            FormPosition = new Point(Cursor.Position.X - Location.X, Cursor.Position.Y - Location.Y);
            //Decir que Esta Clicleado el Formulario
            mouseAction = true;
        }

        private void Logear_MouseUp(object sender, MouseEventArgs e)
        {
            //Si se dejo de Cliclear Desactivar 
            mouseAction = false;
        }
    }
}
