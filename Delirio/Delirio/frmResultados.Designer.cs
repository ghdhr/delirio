﻿namespace Delirio
{
    partial class frmResultados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmResultados));
            this.btn_Exit = new System.Windows.Forms.Button();
            this.lb_Encabezado = new System.Windows.Forms.Label();
            this.lb_Titulo1 = new System.Windows.Forms.Label();
            this.lb_result1 = new System.Windows.Forms.Label();
            this.pbTabla1 = new System.Windows.Forms.PictureBox();
            this.lb_result3 = new System.Windows.Forms.Label();
            this.lb_Titulo3 = new System.Windows.Forms.Label();
            this.lb_result2 = new System.Windows.Forms.Label();
            this.lb_Titulo2 = new System.Windows.Forms.Label();
            this.pbTabla2 = new System.Windows.Forms.PictureBox();
            this.lb_result4 = new System.Windows.Forms.Label();
            this.lb_Titulo4 = new System.Windows.Forms.Label();
            this.lb_result5 = new System.Windows.Forms.Label();
            this.lb_Titulo5 = new System.Windows.Forms.Label();
            this.lb_result6 = new System.Windows.Forms.Label();
            this.lb_Titulo6 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lb_result7 = new System.Windows.Forms.Label();
            this.lb_Titulo7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbTabla1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTabla2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Exit
            // 
            this.btn_Exit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn_Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Exit.Font = new System.Drawing.Font("BigNoodleTitling", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Exit.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.btn_Exit.Location = new System.Drawing.Point(796, 753);
            this.btn_Exit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.Size = new System.Drawing.Size(250, 77);
            this.btn_Exit.TabIndex = 19;
            this.btn_Exit.Text = "Cerrar";
            this.btn_Exit.UseVisualStyleBackColor = true;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // lb_Encabezado
            // 
            this.lb_Encabezado.AutoSize = true;
            this.lb_Encabezado.Font = new System.Drawing.Font("BigNoodleTitling", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Encabezado.Location = new System.Drawing.Point(18, 14);
            this.lb_Encabezado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_Encabezado.Name = "lb_Encabezado";
            this.lb_Encabezado.Size = new System.Drawing.Size(260, 70);
            this.lb_Encabezado.TabIndex = 20;
            this.lb_Encabezado.Text = "Encabezado";
            // 
            // lb_Titulo1
            // 
            this.lb_Titulo1.AutoSize = true;
            this.lb_Titulo1.Font = new System.Drawing.Font("BigNoodleTitling", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Titulo1.Location = new System.Drawing.Point(39, 145);
            this.lb_Titulo1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_Titulo1.Name = "lb_Titulo1";
            this.lb_Titulo1.Size = new System.Drawing.Size(98, 49);
            this.lb_Titulo1.TabIndex = 21;
            this.lb_Titulo1.Text = "Dato1";
            // 
            // lb_result1
            // 
            this.lb_result1.AutoSize = true;
            this.lb_result1.Font = new System.Drawing.Font("BigNoodleTitling", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_result1.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.lb_result1.Location = new System.Drawing.Point(906, 119);
            this.lb_result1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_result1.Name = "lb_result1";
            this.lb_result1.Size = new System.Drawing.Size(135, 70);
            this.lb_result1.TabIndex = 22;
            this.lb_result1.Text = "100%";
            // 
            // pbTabla1
            // 
            this.pbTabla1.Image = ((System.Drawing.Image)(resources.GetObject("pbTabla1.Image")));
            this.pbTabla1.Location = new System.Drawing.Point(30, 130);
            this.pbTabla1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbTabla1.Name = "pbTabla1";
            this.pbTabla1.Size = new System.Drawing.Size(1017, 298);
            this.pbTabla1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbTabla1.TabIndex = 23;
            this.pbTabla1.TabStop = false;
            // 
            // lb_result3
            // 
            this.lb_result3.AutoSize = true;
            this.lb_result3.Font = new System.Drawing.Font("BigNoodleTitling", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_result3.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.lb_result3.Location = new System.Drawing.Point(906, 333);
            this.lb_result3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_result3.Name = "lb_result3";
            this.lb_result3.Size = new System.Drawing.Size(135, 70);
            this.lb_result3.TabIndex = 25;
            this.lb_result3.Text = "100%";
            // 
            // lb_Titulo3
            // 
            this.lb_Titulo3.AutoSize = true;
            this.lb_Titulo3.Font = new System.Drawing.Font("BigNoodleTitling", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Titulo3.Location = new System.Drawing.Point(39, 355);
            this.lb_Titulo3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_Titulo3.Name = "lb_Titulo3";
            this.lb_Titulo3.Size = new System.Drawing.Size(103, 49);
            this.lb_Titulo3.TabIndex = 24;
            this.lb_Titulo3.Text = "Dato3";
            // 
            // lb_result2
            // 
            this.lb_result2.AutoSize = true;
            this.lb_result2.Font = new System.Drawing.Font("BigNoodleTitling", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_result2.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.lb_result2.Location = new System.Drawing.Point(906, 227);
            this.lb_result2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_result2.Name = "lb_result2";
            this.lb_result2.Size = new System.Drawing.Size(135, 70);
            this.lb_result2.TabIndex = 27;
            this.lb_result2.Text = "100%";
            // 
            // lb_Titulo2
            // 
            this.lb_Titulo2.AutoSize = true;
            this.lb_Titulo2.Font = new System.Drawing.Font("BigNoodleTitling", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Titulo2.Location = new System.Drawing.Point(39, 248);
            this.lb_Titulo2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_Titulo2.Name = "lb_Titulo2";
            this.lb_Titulo2.Size = new System.Drawing.Size(103, 49);
            this.lb_Titulo2.TabIndex = 26;
            this.lb_Titulo2.Text = "Dato2";
            // 
            // pbTabla2
            // 
            this.pbTabla2.Image = ((System.Drawing.Image)(resources.GetObject("pbTabla2.Image")));
            this.pbTabla2.Location = new System.Drawing.Point(30, 431);
            this.pbTabla2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbTabla2.Name = "pbTabla2";
            this.pbTabla2.Size = new System.Drawing.Size(1017, 298);
            this.pbTabla2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbTabla2.TabIndex = 28;
            this.pbTabla2.TabStop = false;
            // 
            // lb_result4
            // 
            this.lb_result4.AutoSize = true;
            this.lb_result4.Font = new System.Drawing.Font("BigNoodleTitling", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_result4.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.lb_result4.Location = new System.Drawing.Point(906, 428);
            this.lb_result4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_result4.Name = "lb_result4";
            this.lb_result4.Size = new System.Drawing.Size(135, 70);
            this.lb_result4.TabIndex = 30;
            this.lb_result4.Text = "100%";
            // 
            // lb_Titulo4
            // 
            this.lb_Titulo4.AutoSize = true;
            this.lb_Titulo4.Font = new System.Drawing.Font("BigNoodleTitling", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Titulo4.Location = new System.Drawing.Point(39, 450);
            this.lb_Titulo4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_Titulo4.Name = "lb_Titulo4";
            this.lb_Titulo4.Size = new System.Drawing.Size(103, 49);
            this.lb_Titulo4.TabIndex = 29;
            this.lb_Titulo4.Text = "Dato4";
            // 
            // lb_result5
            // 
            this.lb_result5.AutoSize = true;
            this.lb_result5.Font = new System.Drawing.Font("BigNoodleTitling", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_result5.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.lb_result5.Location = new System.Drawing.Point(906, 534);
            this.lb_result5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_result5.Name = "lb_result5";
            this.lb_result5.Size = new System.Drawing.Size(135, 70);
            this.lb_result5.TabIndex = 32;
            this.lb_result5.Text = "100%";
            // 
            // lb_Titulo5
            // 
            this.lb_Titulo5.AutoSize = true;
            this.lb_Titulo5.Font = new System.Drawing.Font("BigNoodleTitling", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Titulo5.Location = new System.Drawing.Point(39, 556);
            this.lb_Titulo5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_Titulo5.Name = "lb_Titulo5";
            this.lb_Titulo5.Size = new System.Drawing.Size(103, 49);
            this.lb_Titulo5.TabIndex = 31;
            this.lb_Titulo5.Text = "Dato5";
            // 
            // lb_result6
            // 
            this.lb_result6.AutoSize = true;
            this.lb_result6.Font = new System.Drawing.Font("BigNoodleTitling", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_result6.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.lb_result6.Location = new System.Drawing.Point(906, 641);
            this.lb_result6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_result6.Name = "lb_result6";
            this.lb_result6.Size = new System.Drawing.Size(135, 70);
            this.lb_result6.TabIndex = 34;
            this.lb_result6.Text = "100%";
            // 
            // lb_Titulo6
            // 
            this.lb_Titulo6.AutoSize = true;
            this.lb_Titulo6.Font = new System.Drawing.Font("BigNoodleTitling", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Titulo6.Location = new System.Drawing.Point(39, 662);
            this.lb_Titulo6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_Titulo6.Name = "lb_Titulo6";
            this.lb_Titulo6.Size = new System.Drawing.Size(104, 49);
            this.lb_Titulo6.TabIndex = 33;
            this.lb_Titulo6.Text = "Dato6";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(1070, 105);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 35;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Logear_MouseDown);
            this.pictureBox3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Logear_MouseMove);
            this.pictureBox3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Logear_MouseUp);
            // 
            // lb_result7
            // 
            this.lb_result7.AutoSize = true;
            this.lb_result7.Font = new System.Drawing.Font("BigNoodleTitling", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_result7.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.lb_result7.Location = new System.Drawing.Point(900, 700);
            this.lb_result7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_result7.Name = "lb_result7";
            this.lb_result7.Size = new System.Drawing.Size(135, 70);
            this.lb_result7.TabIndex = 37;
            this.lb_result7.Text = "100%";
            // 
            // lb_Titulo7
            // 
            this.lb_Titulo7.AutoSize = true;
            this.lb_Titulo7.Font = new System.Drawing.Font("BigNoodleTitling", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Titulo7.Location = new System.Drawing.Point(33, 721);
            this.lb_Titulo7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_Titulo7.Name = "lb_Titulo7";
            this.lb_Titulo7.Size = new System.Drawing.Size(100, 49);
            this.lb_Titulo7.TabIndex = 36;
            this.lb_Titulo7.Text = "Dato7";
            // 
            // frmResultados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1070, 848);
            this.Controls.Add(this.lb_result7);
            this.Controls.Add(this.lb_Titulo7);
            this.Controls.Add(this.lb_result6);
            this.Controls.Add(this.lb_Titulo6);
            this.Controls.Add(this.lb_result5);
            this.Controls.Add(this.lb_Titulo5);
            this.Controls.Add(this.lb_result4);
            this.Controls.Add(this.lb_Titulo4);
            this.Controls.Add(this.pbTabla2);
            this.Controls.Add(this.lb_result2);
            this.Controls.Add(this.lb_Titulo2);
            this.Controls.Add(this.lb_result3);
            this.Controls.Add(this.lb_Titulo3);
            this.Controls.Add(this.lb_result1);
            this.Controls.Add(this.lb_Titulo1);
            this.Controls.Add(this.lb_Encabezado);
            this.Controls.Add(this.btn_Exit);
            this.Controls.Add(this.pbTabla1);
            this.Controls.Add(this.pictureBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmResultados";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmResultados";
            this.Load += new System.EventHandler(this.frmResultados_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Logear_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Logear_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Logear_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.pbTabla1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTabla2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_Exit;
        private System.Windows.Forms.Label lb_Encabezado;
        private System.Windows.Forms.Label lb_Titulo1;
        private System.Windows.Forms.Label lb_result1;
        private System.Windows.Forms.PictureBox pbTabla1;
        private System.Windows.Forms.Label lb_result3;
        private System.Windows.Forms.Label lb_Titulo3;
        private System.Windows.Forms.Label lb_result2;
        private System.Windows.Forms.Label lb_Titulo2;
        private System.Windows.Forms.PictureBox pbTabla2;
        private System.Windows.Forms.Label lb_result4;
        private System.Windows.Forms.Label lb_Titulo4;
        private System.Windows.Forms.Label lb_result5;
        private System.Windows.Forms.Label lb_Titulo5;
        private System.Windows.Forms.Label lb_result6;
        private System.Windows.Forms.Label lb_Titulo6;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lb_result7;
        private System.Windows.Forms.Label lb_Titulo7;
    }
}