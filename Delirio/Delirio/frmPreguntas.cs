﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech;
using System.Speech.Synthesis;
using System.Speech.Recognition;
using System.Threading;
using IronPython.Hosting;
using System.Diagnostics;
using System.IO;

namespace Delirio
{
    public partial class frmPreguntas : Form
    {
        int i = 0;
        SpeechSynthesizer reader = new SpeechSynthesizer();
        private SpeechRecognitionEngine entradaDeAudio = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("es-ES"));
        public PreguntasDelirio o = new PreguntasDelirio();
        GlobalKeyboardHook gHook;
        //PYTHON INTEGRATION


        public frmPreguntas(string paciente)
        {
            InitializeComponent();
            lb_Paciente.Text = paciente;
        }

        private void frmPreguntas_Load(object sender, EventArgs e)
        {
            Bienvenida();
            lb_Pregunta.Text = o.preguntas[i,0];
            Speech();
            LeerAudio();
            gHook = new GlobalKeyboardHook(); 
                                            
            gHook.KeyDown += new KeyEventHandler(CountSpace);
            foreach (Keys key in Enum.GetValues(typeof(Keys)))
                gHook.HookedKeys.Add(key);

            gHook.hook();
        }
        private void Speech() {
            reader.Dispose();
            reader = new SpeechSynthesizer();
            reader.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult);
            //reader.SelectVoice("Microsoft Server Speech Text to Speech Voice (es-ES, Helena)");
            reader.Speak(o.preguntas[i, 0]);
        }
        private void Bienvenida() {
            reader.Dispose();
            reader = new SpeechSynthesizer();
            reader.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult);
            //reader.SelectVoice("Microsoft Server Speech Text to Speech Voice (es-ES, Helena)");
            lb_Pregunta.Text = o.mensajeBienvenida;
            reader.Speak(o.mensajeBienvenida);
        }
        public PreguntasDelirio Callback() {
            return o;
        }
        public static int ExecuteCommand(string commnd, int timeout)
        {
            var pp = new ProcessStartInfo("cmd.exe", "/C" + commnd)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                WorkingDirectory = "C://users//wiskyi//Documents//Delirio//EmotionINS//",
            };
            var process = Process.Start(pp);
            process.WaitForExit(timeout);
            process.Close();

            return 0;
        }

        private void Emotion() {
            reader.Dispose();
            reader = new SpeechSynthesizer();
            reader.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult);
            lb_Pregunta.Refresh();
            lb_Pregunta.Text = o.emotion;
            lb_Pregunta.Refresh();
            reader.Speak(o.emotion);

            ExecuteCommand("python emotion.py", 110000);
            using (StreamReader sr = new StreamReader("C://users//wiskyi//Documents//Delirio//EmotionINS//values1.txt"))
            {
                // Read the stream to a string, and write the string to the console.
                for (int i = 0; i < 7; i++)
                {
                    o.emotionResult[i] = sr.ReadLine();
                }


            }
            Despedida();
        }
        private void Despedida() {
            reader.Dispose();
            reader = new SpeechSynthesizer();
            reader.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult);
            lb_Pregunta.Refresh();
            lb_Pregunta.Text = o.despedida;
            lb_Pregunta.Refresh();
            reader.Speak(o.despedida);
            Thread.Sleep(5000);
            this.Close();
        }
        private void SpeechSqueezeBottom()
        {
            reader.Dispose();
            reader = new SpeechSynthesizer();
            //gHook.hook();
            reader.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult);
            //reader.SelectVoice("Microsoft Server Speech Text to Speech Voice (es-ES, Helen)"); 
            reader.Speak(o.punto2);
            //lb_Pregunta.Text = o.emotion;
            for (int i = 0; i < 19; i++)
            {
                reader.Speak(o.textoPunto2[i].ToString()+".");
            }
           // Thread.Sleep(13000);
            gHook.unhook();
            Emotion();
        }
        public void CountSpace(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Space)
                   o.countEspacios++;
        }

        private void LeerAudio() {
            try
            {
                //Abrir entrada de Audio
                entradaDeAudio.SetInputToDefaultAudioDevice();
                //Lista de palabras a utilizar
                Choices comandos = new Choices();
                comandos.Add(new string[] { "si", "no" });
                Grammar testGrammar = new Grammar(new GrammarBuilder(comandos));
                
                //Grammar testGrammar = new DictationGrammar();

                entradaDeAudio.LoadGrammar(testGrammar);
                //Leer entrada de audio(lo que recojamos lo va enviar a un metodo )
                entradaDeAudio.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(Lector);
                //Para reconocer varias palabras
                entradaDeAudio.RecognizeAsync(RecognizeMode.Multiple);

            }
            catch (InvalidOperationException e)
            {
                MessageBox.Show("No se puede abrir mas de una vez" +e.ToString());
            }
        }
        private void Lector(object sender, SpeechRecognizedEventArgs e)
        {
            foreach (RecognizedWordUnit palabra in e.Result.Words)
            {
                if (palabra.Text == "si" || palabra.Text == "no") {
                    o.preguntas[i, 2] = palabra.Text;
                    if (i < 3)
                    {
                        i++;
                        lb_Pregunta.Text = o.preguntas[i, 0];
                        Speech();
                    }
                    if(o.preguntas[3, 2] != "") {
                        entradaDeAudio.Dispose();
                        lb_Pregunta.Text = o.punto2;
                        SpeechSqueezeBottom();            
                    }
     
                }
            }
        }
        private void btn_Exit_Click(object sender, EventArgs e)
        {
            gHook.unhook();
            this.Close();
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////    Eventos Del Formulario   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Inicializar Variables
        Point FormPosition;
        Boolean mouseAction;

        private void Logear_MouseMove(object sender, MouseEventArgs e)
        {
            //Si el formulario fue Clicleado
            if (mouseAction == true)
            {
                //A la locación del formulario darle las cordenadas del mouse
                Location = new Point(Cursor.Position.X - FormPosition.X, Cursor.Position.Y - FormPosition.Y);
            }
        }

        private void Logear_MouseDown(object sender, MouseEventArgs e)
        {
            //Darle las cordenadas del mouse a FormPosition
            FormPosition = new Point(Cursor.Position.X - Location.X, Cursor.Position.Y - Location.Y);
            //Decir que Esta Clicleado el Formulario
            mouseAction = true;
        }

        private void Logear_MouseUp(object sender, MouseEventArgs e)
        {
            //Si se dejo de Cliclear Desactivar 
            mouseAction = false;
        }

        private void frmPreguntas_FormClosing(object sender, FormClosingEventArgs e)
        {
            gHook.unhook();
        }
    }
}
