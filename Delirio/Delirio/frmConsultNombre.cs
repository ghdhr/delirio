﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Delirio
{
    public partial class frmConsultNombre : Form
    {
        validacion v = new validacion();
        public PreguntasDelirio preguntas = new PreguntasDelirio();
        public frmConsultNombre()
        {
            InitializeComponent();
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txt_Nombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.Letra(e);
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txt_Nombre.Text != string.Empty)
                {
                    frmPreguntas o = new frmPreguntas(txt_Nombre.Text);
                    this.Visible = false;
                    o.ShowDialog();
                    preguntas = o.Callback();
                    this.Close();
                }

            }

        }

        private void btn_Iniciar_Click(object sender, EventArgs e)
        {
            if (txt_Nombre.Text != string.Empty)
            {
                frmPreguntas o = new frmPreguntas(txt_Nombre.Text);
                this.Visible = false;
                o.ShowDialog();
                preguntas = o.Callback();
                this.Close();
            }
            else {
                MessageBox.Show("No se permite dejar el espacio en blanco", "Error", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }
        public PreguntasDelirio Callback() {
            return preguntas;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////    Eventos Del Formulario   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Inicializar Variables
        Point FormPosition;
        Boolean mouseAction;

        private void Logear_MouseMove(object sender, MouseEventArgs e)
        {
            //Si el formulario fue Clicleado
            if (mouseAction == true)
            {
                //A la locación del formulario darle las cordenadas del mouse
                Location = new Point(Cursor.Position.X - FormPosition.X, Cursor.Position.Y - FormPosition.Y);
            }
        }

        private void Logear_MouseDown(object sender, MouseEventArgs e)
        {
            //Darle las cordenadas del mouse a FormPosition
            FormPosition = new Point(Cursor.Position.X - Location.X, Cursor.Position.Y - Location.Y);
            //Decir que Esta Clicleado el Formulario
            mouseAction = true;
        }

        private void Logear_MouseUp(object sender, MouseEventArgs e)
        {
            //Si se dejo de Cliclear Desactivar 
            mouseAction = false;
        }
    }
}
