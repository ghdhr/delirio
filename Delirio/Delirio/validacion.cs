﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Delirio
{
    class validacion
    {
        public void Letra(KeyPressEventArgs e)
        {
            try
            {//Si es letra

                if (Char.IsLetter(e.KeyChar))
                {//dejar escribir
                    e.Handled = false;
                }//Si es borrar
                else if (Char.IsControl(e.KeyChar))
                {//dejar borrar
                    e.Handled = false;
                }//Si es espacio
                else if (Char.IsSeparator(e.KeyChar))
                {//dejar escribir
                    e.Handled = true;
                }
                else
                {//si no se cumple no habilitar
                    e.Handled = true;
                }
            }
            catch (Exception)
            {

            }
        }

        public void Numero(KeyPressEventArgs e)
        {
            try
            {//si es numero
                if (Char.IsNumber(e.KeyChar))
                {//dejar escribir
                    e.Handled = false;
                }//Si es borrar
                else if (Char.IsControl(e.KeyChar))
                {//dejar borra
                    e.Handled = false;
                }
                else
                {//si no se cumple falso
                    e.Handled = true;
                }
            }
            catch (Exception)
            {

            }

        }
        public void Ene(KeyPressEventArgs e)
        {
            try
            {
                if ('ñ' == e.KeyChar || 'Ñ' == e.KeyChar)
                {
                    e.Handled = true;
                }
                else
                {
                    e.Handled = false;
                }
            }
            catch (Exception)
            {

            }

        }

        public string Acentos(String Palabra)
        {
            Regex a = new Regex("[á|à|ä|â]", RegexOptions.Compiled);
            Regex e = new Regex("[é|è|ë|ê]", RegexOptions.Compiled);
            Regex i = new Regex("[í|ì|ï|î]", RegexOptions.Compiled);
            Regex o = new Regex("[ó|ò|ö|ô]", RegexOptions.Compiled);
            Regex u = new Regex("[ú|ù|ü|û]", RegexOptions.Compiled);
            Palabra = a.Replace(Palabra, "a");
            Palabra = e.Replace(Palabra, "e");
            Palabra = i.Replace(Palabra, "i");
            Palabra = o.Replace(Palabra, "o");
            Palabra = u.Replace(Palabra, "u");
            return Palabra;
        }
    }
}
