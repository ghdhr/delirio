﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IronPython.Hosting;

namespace Delirio
{
    public class PreguntasDelirio
    {
        public string mensajeBienvenida;
        public string[,] preguntas;
        public string punto2;
        public string textoPunto2;
        public string emotion;
        public string despedida;
        public int countEspacios;
        public bool handCheck;
        public string[] emotionResult;
        //Espacio 1 Angry 
        //Espacio 2 Disgust
        //Eapacio 3 Asustado
        //Espacio 4 Happy
        //Espacio 5 Sad
        //Espacio 6 Surprised
        //Espacio 7 Neutral

        public PreguntasDelirio()
        {
            mensajeBienvenida = "Bienvenido. Yo soy Leyda, su cuidadora personal,\n a continuación le haré una serie de preguntas, conteste Si y No";
            preguntas = new string[4, 3]{{"¿Flotará una piedra sobre el agua?","no",""},
                                      {"¿Hay peces en el mar?","si",""},
                                      {"¿1 libra pesa más de 2 libras?","no",""},
                                      {"¿Puedes usar un martillo para clavar un clavo?","si",""}};
            punto2 = "Te voy a leer una serie de 10 letras.\nCada vez que escuchas la letra A, apriete el espacio.";
            textoPunto2 = "o e i a e i a u a e";
            emotion = "A continuación se analizarán sus emociones,\n espere por favor...";
            despedida = "Muchas gracias por su tiempo,\n el test a finalizado.";
            emotionResult = new string[7];
        }
        
    }
}
