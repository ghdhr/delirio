﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Delirio
{
    public partial class frmResultados : Form
    {
        PreguntasDelirio o = new PreguntasDelirio();
        public frmResultados(string encabezado, PreguntasDelirio oPreguntasDelirio)
        {
            InitializeComponent();
            lb_Encabezado.Text = encabezado;
            o = oPreguntasDelirio;
        }

        private void frmResultados_Load(object sender, EventArgs e)
        {
            switch (lb_Encabezado.Text) {
                case "Preguntas":                  
                    for (int i = 0; i < 5; i++)
                    {
                        switch (i) {
                            case 0:
                                lb_Titulo1.Text = o.preguntas[i, 0];
                                lb_result1.Text = o.preguntas[i, 2];
                                if (o.preguntas[i, 1] != o.preguntas[i, 2])
                                    lb_result1.ForeColor = Color.DarkRed;
                                break;
                            case 1:
                                lb_Titulo2.Text = o.preguntas[i, 0];
                                lb_result2.Text = o.preguntas[i, 2];
                                if (o.preguntas[i, 1] != o.preguntas[i, 2])
                                    lb_result2.ForeColor = Color.DarkRed;
                                break;
                            case 2:
                                lb_Titulo3.Text = o.preguntas[i, 0];
                                lb_result3.Text = o.preguntas[i, 2];
                                if (o.preguntas[i, 1] != o.preguntas[i, 2])
                                    lb_result3.ForeColor = Color.DarkRed;
                                break;
                            case 3:
                                lb_Titulo4.Text = o.preguntas[i, 0];
                                lb_result4.Text = o.preguntas[i, 2];
                                if (o.preguntas[i, 1] != o.preguntas[i, 2])
                                    lb_result4.ForeColor = Color.DarkRed;
                                break;
                            case 4:
                                lb_Titulo6.Text = "Cantidad de Espacios precionados:";
                                lb_result6.Text = o.countEspacios+"";
                                break;
                        }
                    }

                    pbTabla2.Visible = false;
                    lb_Titulo5.Visible = false;
                    lb_result5.Visible = false;

                    break;
                case "Mover las manos":
                    lb_Titulo1.Text = "Reacciono Bien con las manos?";
                    lb_result1.Text = "si";

                    pbTabla1.Visible = false;
                    pbTabla2.Visible = false;

                    lb_Titulo2.Visible = false;
                    lb_result2.Visible = false;


                    lb_Titulo3.Visible = false;
                    lb_result3.Visible = false;


                    lb_Titulo4.Visible = false;
                    lb_result4.Visible = false;

                    lb_Titulo5.Visible = false;
                    lb_result5.Visible = false;

                    lb_Titulo6.Visible = false;
                    lb_result6.Visible = false;
                    break;
                case "Emociones":
                    lb_Titulo1.Text = "Enojado";
                    lb_Titulo2.Text = "Disgustado";
                    lb_Titulo3.Text = "Feliz";
                    lb_Titulo4.Text = "Triste";
                    lb_Titulo5.Text = "Sorprendido";
                    lb_Titulo6.Text = "Neutral";


                    lb_result1.Text = o.emotionResult[0].Substring(0,5);
                    lb_result2.Text = o.emotionResult[1].Substring(0, 5);
                    lb_result3.Text = o.emotionResult[2].Substring(0, 5);
                    lb_result4.Text = o.emotionResult[3].Substring(0, 5);
                    lb_result5.Text = o.emotionResult[4].Substring(0, 5);
                    lb_result6.Text = o.emotionResult[5].Substring(0, 5);
                    lb_result7.Text = o.emotionResult[6].Substring(0, 5);
                    break;
                
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////    Eventos Del Formulario   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Inicializar Variables
        Point FormPosition;
        Boolean mouseAction;

        private void Logear_MouseMove(object sender, MouseEventArgs e)
        {
            //Si el formulario fue Clicleado
            if (mouseAction == true)
            {
                //A la locación del formulario darle las cordenadas del mouse
                Location = new Point(Cursor.Position.X - FormPosition.X, Cursor.Position.Y - FormPosition.Y);
            }
        }

        private void Logear_MouseDown(object sender, MouseEventArgs e)
        {
            //Darle las cordenadas del mouse a FormPosition
            FormPosition = new Point(Cursor.Position.X - Location.X, Cursor.Position.Y - Location.Y);
            //Decir que Esta Clicleado el Formulario
            mouseAction = true;
        }

        private void Logear_MouseUp(object sender, MouseEventArgs e)
        {
            //Si se dejo de Cliclear Desactivar 
            mouseAction = false;
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
