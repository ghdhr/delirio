from keras.preprocessing.image import img_to_array
import imutils
import cv2
from keras.models import load_model
import numpy as np
import time

class Emotion(object):
        def create_txt(self, values):
                f= open("values1.txt","w+")
                for index in range(7):
                    f.write(str(values[index])+"\n")
                f.close()
                
        def exec_emotion(self):
                detection_model_path = 'haarcascade_files/haarcascade_frontalface_default.xml'
                emotion_model_path = 'models/_mini_XCEPTION.106-0.65.hdf5'
                face_detection = cv2.CascadeClassifier(detection_model_path)
                emotion_classifier = load_model(emotion_model_path, compile=False)
                EMOTIONS = ["angry" ,"disgust","scared", "happy", "sad", "surprised","neutral"]
                values = [0, 0, 0, 0, 0, 0, 0]
                camera = cv2.VideoCapture(1)
                #Este es el valor en segundos del analisis total
                final_time = 10
                lap = time.time()
                wall_time = time.time()
                try:
                        while lap - wall_time < final_time:
                                frame = camera.read()[1]
                                frame = imutils.resize(frame,width=400)
                                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                                faces = face_detection.detectMultiScale(gray,scaleFactor=1.1,minNeighbors=5,minSize=(30,30),flags=cv2.CASCADE_SCALE_IMAGE)
                                frameClone = frame.copy()
                                if len(faces) > 0:
                                        faces = sorted(faces, reverse=True,key=lambda x: (x[2] - x[0]) * (x[3] - x[1]))[0]
                                        (fX, fY, fW, fH) = faces
                                        roi = gray[fY:fY + fH, fX:fX + fW]
                                        roi = cv2.resize(roi, (48, 48))
                                        roi = roi.astype("float") / 255.0
                                        roi = img_to_array(roi)
                                        roi = np.expand_dims(roi, axis=0)
                                        preds = emotion_classifier.predict(roi)[0]
                                for (i, (emotion, prob)) in enumerate(zip(EMOTIONS, preds)):
                                        if(values[i]!= 0):
                                                values[i] = (values[i]+prob*100)/2
                                        else:
                                                values[i] = prob*100
                                lap = time.time()
                except Exception:
                        print("Something went wrong ")
                finally:
                        cv2.destroyAllWindows()
                        camera.release()
                        self.create_txt(values)

if __name__ == "__main__":
    emotion = Emotion()
    emotion.exec_emotion()