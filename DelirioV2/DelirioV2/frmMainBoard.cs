﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DelirioV2
{
    public partial class frmMainBoard : Form
    {
        public frmMainBoard()
        {
            InitializeComponent();
        }

        private void FrmMainBoard_Load(object sender, EventArgs e)
        {
            lbHora.Text = DateTime.Now.ToShortTimeString();
            timer1.Start();
        }

        private void FrmMainBoard_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PbMedicion_Click(object sender, EventArgs e)
        {
            frmMedirDelirio o = new frmMedirDelirio();
            o.ShowDialog();
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            lbHora.Text = DateTime.Now.ToShortTimeString();
        }
    }
}
