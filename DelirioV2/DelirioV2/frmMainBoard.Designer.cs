﻿namespace DelirioV2
{
    partial class frmMainBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainBoard));
            this.pbTerapia = new System.Windows.Forms.PictureBox();
            this.pbMedicion = new System.Windows.Forms.PictureBox();
            this.pbVentana = new System.Windows.Forms.PictureBox();
            this.lbTerapia = new System.Windows.Forms.Label();
            this.lbVentana = new System.Windows.Forms.Label();
            this.lbMedicion = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lbHora = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbTerapia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMedicion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbVentana)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // pbTerapia
            // 
            this.pbTerapia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbTerapia.Image = ((System.Drawing.Image)(resources.GetObject("pbTerapia.Image")));
            this.pbTerapia.Location = new System.Drawing.Point(1046, 563);
            this.pbTerapia.Name = "pbTerapia";
            this.pbTerapia.Size = new System.Drawing.Size(428, 419);
            this.pbTerapia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbTerapia.TabIndex = 2;
            this.pbTerapia.TabStop = false;
            // 
            // pbMedicion
            // 
            this.pbMedicion.Image = ((System.Drawing.Image)(resources.GetObject("pbMedicion.Image")));
            this.pbMedicion.Location = new System.Drawing.Point(255, 587);
            this.pbMedicion.Name = "pbMedicion";
            this.pbMedicion.Size = new System.Drawing.Size(379, 363);
            this.pbMedicion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbMedicion.TabIndex = 3;
            this.pbMedicion.TabStop = false;
            this.pbMedicion.Click += new System.EventHandler(this.PbMedicion_Click);
            // 
            // pbVentana
            // 
            this.pbVentana.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pbVentana.Image = ((System.Drawing.Image)(resources.GetObject("pbVentana.Image")));
            this.pbVentana.Location = new System.Drawing.Point(670, 578);
            this.pbVentana.Name = "pbVentana";
            this.pbVentana.Size = new System.Drawing.Size(390, 391);
            this.pbVentana.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbVentana.TabIndex = 4;
            this.pbVentana.TabStop = false;
            // 
            // lbTerapia
            // 
            this.lbTerapia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTerapia.AutoSize = true;
            this.lbTerapia.BackColor = System.Drawing.Color.Transparent;
            this.lbTerapia.Font = new System.Drawing.Font("Intel Clear W Light", 19.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTerapia.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(134)))), ((int)(((byte)(164)))));
            this.lbTerapia.Location = new System.Drawing.Point(1075, 954);
            this.lbTerapia.Name = "lbTerapia";
            this.lbTerapia.Size = new System.Drawing.Size(340, 65);
            this.lbTerapia.TabIndex = 5;
            this.lbTerapia.Text = "Terapia Física";
            // 
            // lbVentana
            // 
            this.lbVentana.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbVentana.AutoSize = true;
            this.lbVentana.BackColor = System.Drawing.Color.Transparent;
            this.lbVentana.Font = new System.Drawing.Font("Intel Clear W Light", 19.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVentana.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(134)))), ((int)(((byte)(164)))));
            this.lbVentana.Location = new System.Drawing.Point(680, 955);
            this.lbVentana.Name = "lbVentana";
            this.lbVentana.Size = new System.Drawing.Size(370, 65);
            this.lbVentana.TabIndex = 6;
            this.lbVentana.Text = "Modo Ventana";
            // 
            // lbMedicion
            // 
            this.lbMedicion.AutoSize = true;
            this.lbMedicion.BackColor = System.Drawing.Color.Transparent;
            this.lbMedicion.Font = new System.Drawing.Font("Intel Clear W Light", 19.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMedicion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(134)))), ((int)(((byte)(164)))));
            this.lbMedicion.Location = new System.Drawing.Point(204, 954);
            this.lbMedicion.Name = "lbMedicion";
            this.lbMedicion.Size = new System.Drawing.Size(478, 65);
            this.lbMedicion.TabIndex = 7;
            this.lbMedicion.Text = "Medición de Delirio";
            this.lbMedicion.Click += new System.EventHandler(this.PbMedicion_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Location = new System.Drawing.Point(1602, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(156, 127);
            this.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnExit.TabIndex = 0;
            this.btnExit.TabStop = false;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(110, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(300, 120);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(47, 58);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(155, 136);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 9;
            this.pictureBox5.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // lbHora
            // 
            this.lbHora.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbHora.AutoSize = true;
            this.lbHora.BackColor = System.Drawing.Color.Transparent;
            this.lbHora.Font = new System.Drawing.Font("Intel Clear W Light", 19.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHora.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(134)))), ((int)(((byte)(164)))));
            this.lbHora.Location = new System.Drawing.Point(1496, 980);
            this.lbHora.Name = "lbHora";
            this.lbHora.Size = new System.Drawing.Size(249, 65);
            this.lbHora.TabIndex = 10;
            this.lbHora.Text = "12:59 am";
            // 
            // frmMainBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1770, 1056);
            this.Controls.Add(this.lbHora);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbMedicion);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lbVentana);
            this.Controls.Add(this.lbTerapia);
            this.Controls.Add(this.pbVentana);
            this.Controls.Add(this.pbMedicion);
            this.Controls.Add(this.pbTerapia);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMainBoard";
            this.Text = "z";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMainBoard_FormClosing);
            this.Load += new System.EventHandler(this.FrmMainBoard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbTerapia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMedicion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbVentana)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pbTerapia;
        private System.Windows.Forms.PictureBox pbMedicion;
        private System.Windows.Forms.PictureBox pbVentana;
        private System.Windows.Forms.Label lbTerapia;
        private System.Windows.Forms.Label lbVentana;
        private System.Windows.Forms.Label lbMedicion;
        private System.Windows.Forms.PictureBox btnExit;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lbHora;
    }
}