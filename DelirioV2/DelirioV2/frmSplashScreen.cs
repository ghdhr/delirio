﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DelirioV2
{
    public partial class frmSplashScreen : Form
    {
        int i = 0;
        int j = 0;
        public frmSplashScreen()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            switch (i) {
                case 0:
                    lbLoading.Text = "Iniciando";
                break;
                case 1:
                    lbLoading.Text = "Iniciando.";
                break;
                case 2:
                    lbLoading.Text = "Iniciando..";
                break;
                case 3:
                    lbLoading.Text = "Iniciando..";
                break;
            }
            i++;
            j++;
            if (i >= 3) i = 0;
            if (j >= 5)
            {
                timer1.Stop();
                frmMainBoard o = new frmMainBoard();
                o.Show();
                this.Visible = false;
            }
        }
    }
}
