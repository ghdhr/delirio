﻿namespace DelirioV2
{
    partial class frmMedirDelirio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.PictureBox();
            this.pbLeida = new System.Windows.Forms.PictureBox();
            this.lbOutput = new System.Windows.Forms.Label();
            this.lbProgressbar = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLeida)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.Location = new System.Drawing.Point(1249, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(156, 127);
            this.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnExit.TabIndex = 1;
            this.btnExit.TabStop = false;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // pbLeida
            // 
            this.pbLeida.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pbLeida.Image = global::DelirioV2.Properties.Resources._373_2_;
            this.pbLeida.Location = new System.Drawing.Point(524, 360);
            this.pbLeida.Name = "pbLeida";
            this.pbLeida.Size = new System.Drawing.Size(357, 345);
            this.pbLeida.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLeida.TabIndex = 10;
            this.pbLeida.TabStop = false;
            // 
            // lbOutput
            // 
            this.lbOutput.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbOutput.AutoSize = true;
            this.lbOutput.BackColor = System.Drawing.Color.Transparent;
            this.lbOutput.Font = new System.Drawing.Font("Intel Clear W Light", 25.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(134)))), ((int)(((byte)(164)))));
            this.lbOutput.Location = new System.Drawing.Point(262, 99);
            this.lbOutput.Name = "lbOutput";
            this.lbOutput.Size = new System.Drawing.Size(884, 168);
            this.lbOutput.TabIndex = 11;
            this.lbOutput.Text = "Bienvenido yo soy Leyda, su\r\ncuidadora virtual.";
            this.lbOutput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbProgressbar
            // 
            this.lbProgressbar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lbProgressbar.AutoSize = true;
            this.lbProgressbar.BackColor = System.Drawing.Color.Transparent;
            this.lbProgressbar.Font = new System.Drawing.Font("Intel Clear W Light", 19.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProgressbar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(134)))), ((int)(((byte)(164)))));
            this.lbProgressbar.Location = new System.Drawing.Point(436, 703);
            this.lbProgressbar.Name = "lbProgressbar";
            this.lbProgressbar.Size = new System.Drawing.Size(0, 65);
            this.lbProgressbar.TabIndex = 12;
            this.lbProgressbar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmMedirDelirio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1417, 895);
            this.Controls.Add(this.lbProgressbar);
            this.Controls.Add(this.lbOutput);
            this.Controls.Add(this.pbLeida);
            this.Controls.Add(this.btnExit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMedirDelirio";
            this.Text = "frmMedirDelirio";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMedirDelirio_FormClosing);
            this.Load += new System.EventHandler(this.FrmMedirDelirio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLeida)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox btnExit;
        private System.Windows.Forms.PictureBox pbLeida;
        public System.Windows.Forms.Label lbOutput;
        public System.Windows.Forms.Label lbProgressbar;
    }
}