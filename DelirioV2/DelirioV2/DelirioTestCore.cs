﻿using System.Speech.Synthesis;
using System.Speech.Recognition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Diagnostics;

namespace DelirioV2
{
    public class DelirioTestCore
    {
        public string mensajeBienvenida;
        public string[,] preguntas;
        public string punto2;
        public string textoPunto2;
        public string despedida;
        public int countEspacios;
        public string[,] emotionResult;
        public int countPreguntas;

        SpeechSynthesizer reader = new SpeechSynthesizer();
        private SpeechRecognitionEngine entradaDeAudio = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("es-ES"));
        GlobalKeyboardHook gHook;
        public DelirioTestCore()
        {
            preguntas = new string[4, 3]{{"¿Flotará una piedra sobre el\nagua?","no",""},
                                      {"¿Hay peces en el mar?","si",""},
                                      {"¿1 libra pesa más\nde 2 libras?","no",""},
                                      {"¿Puedes usar un martillo para\nclavar un clavo?","si",""}};
            punto2 = "Te voy a leer una serie de 10 letras.\nCada vez que escuchas la letra A, apriete el espacio.";
            textoPunto2 = "o e i a e i a u a e";
            despedida = "Muchas gracias por su tiempo,\n el test a finalizado.";
            emotionResult = new string[7, 2] {{ "Enojado", "" },
                                             { "Disgustado", "" },
                                             { "Asustado", "" },
                                             { "Feliz", "" },
                                             { "Triste", "" },
                                             { "Sorprendido", "" },
                                             { "Neutral", "" }};
        }
        public void Stop() {
            reader.SpeakAsyncCancelAll();
            entradaDeAudio.Dispose();
        }
        public void Start(ref Label lbOutput, ref Label lbProgressbar, ref PictureBox pbLeyda) {
         
            lbOutput.Refresh();
            lbOutput.Text = Bienvenida();
            //Emotion
            lbOutput.Refresh();
            lbOutput.Text = Emotion();
            lbOutput.Refresh();
            lbProgressbar.Text = "Analizando. \nTiempo estimado 1 min.";
            lbProgressbar.Refresh();
            pbLeyda.Image = DelirioV2.Properties.Resources._373_3_;
            pbLeyda.Refresh();
            int progressBarCount = 0;
            int porcentaje = 0;
            for (int i = 1; i < 240; i++)
            {
                porcentaje = (i*100/240);
                Thread.Sleep(300);
                switch (progressBarCount) {
                    case 0:
                        lbProgressbar.Text = "Analizando. ("+porcentaje+"%)\nTiempo estimado 1 min.";
                        break;
                    case 1:
                        lbProgressbar.Text = "Analizando.. (" + porcentaje + "%)\nTiempo estimado 1 min.";
                        break;
                    case 2:
                        lbProgressbar.Text = "Analizando... (" + porcentaje + "%)\nTiempo estimado 1 min.";
                        break;
                    case 3:
                        lbProgressbar.Text = "Analizando (" + porcentaje + "%)\nTiempo estimado 1 min.";
                    break;
                }
                if (progressBarCount <= 3)progressBarCount++;
                else progressBarCount = 0;
                lbProgressbar.Refresh();
            }
            lbProgressbar.Text = "";
            lbProgressbar.Refresh();
            pbLeyda.Image = DelirioV2.Properties.Resources._373_2_;
            pbLeyda.Refresh();
 
            //Preguntas
            lbOutput.Refresh();
            lbOutput.Text = Preguntas(-1);
            Thread.Sleep(6000);
            lbOutput.Text = Preguntas(0);
            Thread.Sleep(1000);
            PreguntasIniciarLector();


        }
        private void SpeechSqueezeBottom()
        {

            Speak("Te voy a leer una serie de 10 letras.\nCada vez que escuchas la letra A, apriete el espacio.");
            //lb_Pregunta.Text = o.emotion;
            for (int i = 0; i < 19; i++)
            {
                reader.Speak(textoPunto2[i].ToString() + ".");
            }
            // Thread.Sleep(13000);
            gHook.unhook();
        }
        private void PreguntasIniciarLector()
        {
            try
            {
                entradaDeAudio.SetInputToDefaultAudioDevice();
                Choices comandos = new Choices();
                comandos.Add(new string[] { "si", "no" });
                Grammar testGrammar = new Grammar(new GrammarBuilder(comandos));
                entradaDeAudio.LoadGrammar(testGrammar);
                entradaDeAudio.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(Lector);
                //entradaDeAudio.RecognizeAsync(RecognizeMode.Multiple);
                entradaDeAudio.RecognizeAsync(RecognizeMode.Multiple);
            }
            catch (InvalidOperationException e)
            {
                MessageBox.Show("No se puede abrir mas de una vez" + e.ToString());
            }
        }
        private void Lector(object sender, SpeechRecognizedEventArgs e)
        {
            foreach (RecognizedWordUnit palabra in e.Result.Words)
            {
                if (palabra.Text == "si" || palabra.Text == "no")
                {
                    if (countPreguntas < 3)
                    {
                        countPreguntas++;
                        Preguntas(countPreguntas);
                    }
                    if (preguntas[3, 2] != "")
                    {
                       // entradaDeAudio.Dispose();                    
                        SpeechSqueezeBottom();
                    }

                }
            }
        }
        private String Preguntas(int i) {
            string mensaje = "A continuación se le hará una\nserie de preguntas,\n Conteste Si y NO.";
            if (i > -1)
            mensaje = preguntas[i, 0];
            Speak(mensaje);
            return mensaje;
        }
        private void EmotionAnalize() {
            ExecuteCommand("python emotion.py", 110000);
            using (StreamReader sr = new StreamReader("C://users//wiskyi//Documents//Delirio//EmotionINS//values1.txt"))
            {
                // Read the stream to a string, and write the string to the console.
                for (int i = 0; i < 7; i++)
                {
                    emotionResult[i, 1] = sr.ReadLine();
                }
            }
        }
        private string Emotion() {
            string mensaje = "A continuación se analizarán\nsus emociones,\n Mire a la cámara por favor.";
            Speak("A continuación se analizarán sus emociones, Mire a la cámara por favor");
            Thread th = new Thread(EmotionAnalize);
            th.Start();
            return mensaje;
        }
        public static int ExecuteCommand(string commnd, int timeout)
        {
            var pp = new ProcessStartInfo("cmd.exe", "/C" + commnd)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                WorkingDirectory = "C://users//wiskyi//Documents//Delirio//EmotionINS//",
            };
            var process = Process.Start(pp);
            process.WaitForExit(timeout);
            process.Close();

            return 0;
        }
        private string Bienvenida()
        {
            string mensaje = "Bienvenido yo soy Leyda, su\ncuidadora virtual.";
            Speak("Bienvenido. yo soy Leyda, su cuidadora virtual.");
            Thread.Sleep(5000);
            return mensaje;   
        }
        private void Speak(string mensaje)
        {
            reader.Dispose();
            reader = new SpeechSynthesizer();
            reader.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult);
            reader.SpeakAsync(mensaje);

        }
 


    }
}
