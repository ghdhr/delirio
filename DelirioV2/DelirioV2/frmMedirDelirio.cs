﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DelirioV2
{
    public partial class frmMedirDelirio : Form
    {
        public DelirioTestCore test = new DelirioTestCore();
        Thread th;
        public frmMedirDelirio()
        {
            InitializeComponent();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmMedirDelirio_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;            
            th = new Thread(AsyncTest);
            th.Start();
        }
        
        public void AsyncTest() {
            test.Start(lbOutput: ref lbOutput, lbProgressbar: ref lbProgressbar, ref pbLeida);
        }

        private void FrmMedirDelirio_FormClosing(object sender, FormClosingEventArgs e)
        {
            test.Stop();
            th.Abort();
        }
    }
}
